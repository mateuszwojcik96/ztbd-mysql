package mateuszwojcik.ztbd.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Performance {
    @Id
    @GeneratedValue
    private int iteration;
    LocalDateTime creationDateTime;
    private long memoryFree;
}
