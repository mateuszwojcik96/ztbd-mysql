package mateuszwojcik.ztbd.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Sds {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String id2;
    private String sensorId;
    private String location;
    private String lat;
    private String lon;
    private String timestamp;
    private String p1;
    private String p2;

    public Sds(String id2, String sensorId, String location, String lat, String lon, String timestamp, String p1, String p2) {
        this.id2 = id2;
        this.sensorId = sensorId;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.timestamp = timestamp;
        this.p1 = p1;
        this.p2 = p2;
    }
}
