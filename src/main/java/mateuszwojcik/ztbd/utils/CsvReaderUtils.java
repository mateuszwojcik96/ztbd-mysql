package mateuszwojcik.ztbd.utils;

import lombok.extern.slf4j.Slf4j;
import mateuszwojcik.ztbd.entity.Sds;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


@Slf4j
public class CsvReaderUtils {

    public List<Sds> readRecordsFromCsvFileMultiThreads(String filePath) {

        List<Sds> sds = new ArrayList<>();
        try {
            
            File inputF = new File(filePath);
            InputStream inputStream = new FileInputStream(inputF);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            sds = bufferedReader.lines().skip(1).map(mapToItem).collect(Collectors.toList());
            bufferedReader.close();
            return sds;

        } catch (Exception e){
            return sds;
        }
    }

    private Function<String, Sds> mapToItem = (line) -> {
        String[] p = line.split(",");
        Sds item;
        try {
        item = new Sds(p[0], p[1], p[2], p[3], p[4] , p[5], p[6], p[7]);
        } catch (Exception e) {
            item = new Sds(p[0], p[1], p[2], p[3], p[4] , p[5], "", "");
        }
        return item;
    };

}
