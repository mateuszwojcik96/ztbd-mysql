package mateuszwojcik.ztbd.repository;

import mateuszwojcik.ztbd.entity.Sds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SdsRepository extends JpaRepository<Sds, Integer> {
    List<Sds> findAllBySensorId(String sensorId);
    @Modifying
    @Query("update Sds s set s.p1 = ?1 where s.sensorId = ?2")
    void setP1BySensorId(String newP1, String sensorId);
    void deleteBySensorId(String id);
}
