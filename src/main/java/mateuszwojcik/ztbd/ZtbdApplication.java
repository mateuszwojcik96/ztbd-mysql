package mateuszwojcik.ztbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZtbdApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZtbdApplication.class, args);
    }

}
