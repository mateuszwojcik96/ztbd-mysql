package mateuszwojcik.ztbd.cotroller;

import mateuszwojcik.ztbd.entity.Performance;
import mateuszwojcik.ztbd.entity.Sds;
import mateuszwojcik.ztbd.repository.PerformanceRepository;
import mateuszwojcik.ztbd.repository.SdsRepository;
import mateuszwojcik.ztbd.utils.CsvReaderUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@Transactional
@RequestMapping("/api")
public class ZtbdController {

    private SdsRepository sdsRepository;
    private CsvReaderUtils csvReaderUtils;
    private PerformanceRepository performanceRepository;

    public ZtbdController(SdsRepository sdsRepository, PerformanceRepository performanceRepository) {
        this.sdsRepository = sdsRepository;
        this.csvReaderUtils = new CsvReaderUtils();
        this.performanceRepository = performanceRepository;
    }

    @PostMapping("/add/sds")
    public String addSds() throws IOException {
        String path = "C:\\Users\\mwojc\\Documents\\studia\\ZTBD\\sofia-air-quality-dataset\\2017-12_sds011sof.csv";
        List<Sds> sdsList = csvReaderUtils.readRecordsFromCsvFileMultiThreads(path);


        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for(int i = 0; i< 597803; i ++){
            int finalI = i;

            executorService.execute(()->{
                sdsRepository.save(sdsList.get(finalI));
//                performanceRepository.save(new Performance(finalI, LocalDateTime.now(), (runtime.freeMemory() / 1024)));
            });
        }

        executorService.shutdown();
        return "OK";
    }

    @GetMapping("/find")
    public ResponseEntity<?> findById(@RequestParam String id) {
        return new ResponseEntity<>(sdsRepository.findAllBySensorId(id), HttpStatus.OK);
    }


    @PutMapping("/update")
    public ResponseEntity<?> editById(@RequestParam String id, @RequestParam String newP1) {
        sdsRepository.setP1BySensorId(newP1, id);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteById(@RequestParam String id) {
        sdsRepository.deleteBySensorId(id);

        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}
